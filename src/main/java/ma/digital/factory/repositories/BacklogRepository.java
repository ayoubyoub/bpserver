package ma.digital.factory.repositories;

import ma.digital.factory.domain.Backlog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BacklogRepository extends CrudRepository<Backlog, Long> {

    Backlog getById(Long Identifier);
}
