# Plugins
=================================
Name: Apache Conf
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=mrmlnc.vscode-apache
=================================
Name: Auto Import
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=steoates.autoimport
=================================
Name: Auto Rename Tag
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag
=================================
Name: Better Comments
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments
=================================
Name: Bracket Pair Colorizer
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer
=================================
Name: Cloudfoundry Manifest YML Support
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=Pivotal.vscode-manifest-yaml
=================================
Name: Color Picker
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=anseki.vscode-color
=================================
Name: Concourse CI Pipeline Editor
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=Pivotal.vscode-concourse
=================================
Name: Debugger for Chrome
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome
=================================
Name: Debugger for Java
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-debug
=================================
Name: Docker
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker
=================================
Name: ES7 React/Redux/GraphQL/React-Native snippets
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets
=================================
Name: ESLint
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
=================================
Name: Excel to Markdown table
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=csholmq.excel-to-markdown-table
=================================
Name: Flow Language Support
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=flowtype.flow-for-vscode
=================================
Name: GitLens — Git supercharged
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens
=================================
Name: IntelliSense for CSS class names in HTML
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=Zignd.html-css-class-completion
=================================
Name: Java Dependency Viewer
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-dependency
=================================
Name: Java Dependency Viewer
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-dependency
=================================
Name: Java Test Runner
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-test
=================================
Name: Jest Snippets
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=andys8.jest-snippets
=================================
Name: Language Support for Java(TM) by Red Hat
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=redhat.java
=================================
Name: Live Server
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer
=================================
Name: Material Icon Theme
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme
=================================
Name: Maven for Java
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-maven
=================================
Name: Path Intellisense
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense
=================================
Name: PrintCode
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=nobuhito.printcode
=================================
Name: Project Manager
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=alefragnani.project-manager
=================================
Name: Quokka.js
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=WallabyJs.quokka-vscode
=================================
Name    :   REST Client
Execute :   cmd + alt + R
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=humao.rest-client
=================================
Name: Sass
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=robinbentley.sass-indented
=================================
Name: Slack Theme
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=felipe-mendes.slack-theme
=================================
Name: Spring Boot Dashboard
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-spring-boot-dashboard
=================================
Name: Spring Boot Extension Pack
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=Pivotal.vscode-boot-dev-pack
=================================
Name: Spring Boot Tools
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=Pivotal.vscode-spring-boot
=================================
Name: Spring Initializr Java Support
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-spring-initializr
=================================
Name: SQL Server (mssql)
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ms-mssql.mssql
=================================
Name    :   SQLTools
Fixe    :   ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Casacasa123'
Execute :   cmd + E
VS Marketplace Link : https://marketplace.visualstudio.com/items?itemName=mtxr.sqltools
=================================
Name: stylelint
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=shinnn.stylelint
=================================
Name: TSLint (deprecated)
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=eg2.tslint
=================================
Name: Version Lens
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=pflannery.vscode-versionlens
=================================
Name: Visual Studio IntelliCode
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=VisualStudioExptTeam.vscodeintellicode
=================================
Name: vscode-flow-ide
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=gcazaciuc.vscode-flow-ide
=================================
Name: vscode-icons
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons
=================================